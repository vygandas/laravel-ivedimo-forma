<?php

namespace App\Http\Controllers;

use App\Post;
use App\Topic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $topics = Topic::orderBy('created_at', 'desc')->get();
        return view('home')
            ->with('topics', $topics);
    }


    public function postFormData(Request $request) {
        // Taip surenkami visi atsiųsti duomenys
        $formData = $request->all();

        // Sukuriamas validatorius. Daugiau info apie galimus validavimo tipus
        // https://laravel.com/docs/5.6/validation#available-validation-rules
        $validator = Validator::make($formData, [
            'message' => 'required|min:10',
        ]);

        // Tikrinama ar validacija sėkminga. Šiuo atveju, jei validacija nesėkminga -
        // grąžiname vartotoją atgal į formos langą ir nusiunčiame klaidų pranešimus į
        // View failą (blade / html)
        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        // Tiesiog spausdinimas pažiūrėjimui
        dd($formData);
    }

    public function createTopic(Request $request) {
        $name = $request->input('topic_name', null);
        Topic::create([
            'user_id' => auth()->id(),
            'name' => $name
        ]);
        return redirect()
            ->back();
    }

    public function topic($id) {
        $topic = Topic::where('id', $id)->first();
        if (empty($topic)) { return redirect('home'); }

        $posts = Post::where('topic_id', $topic->id)
            ->paginate(3);

        return view('topic')
            ->with('topic', $topic)
            ->with('posts', $posts);
    }

    public function postPost(Request $request) {
        $post = new Post();
        $post->user_id = auth()->id();
        $post->topic_id = $request->input('topic_id');
        $post->name = $request->input('name');
        $post->save();
        return redirect()->back();
    }

}
