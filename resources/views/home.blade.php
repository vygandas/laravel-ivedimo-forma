@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Forumas</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif


                    <form action="{{ route('createTopic') }}" method="post">
                        @csrf
                        <label>
                            Įveskite naujos temos pavadinimą
                            <input name="topic_name" class="form-control" placeholder="Pavadinimas">
                        </label>
                            <button type="submit" class="btn btn-success">
                                Sukurti
                            </button>
                    </form>


                    <table class="table mt-5">
                        <thead>
                            <th>Temos</th>
                            <th>Data</th>
                        </thead>
                        <tbody>
                        @foreach($topics as $topic)
                            <tr>
                                <td>
                                    <a href="{{ route('topic', [ 'id' => $topic->id ]) }}">
                                        {{ $topic->name }}
                                    </a>
                                </td>
                                <td>
                                    {{ $topic->created_at->timezone('Europe/Vilnius') }}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
