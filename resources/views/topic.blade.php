@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Tema: {{ $topic->name }}</div>

                    <div class="card-body">

                        <table class="table table-bordered">
                            @foreach($posts as $post)
                                <tr>
                                    <td>
                                        {{ $post->name }}
                                    </td>
                                </tr>
                            @endforeach
                        </table>

                        <div class="text-center my-3">
                            {!! $posts->render() !!}
                        </div>

                        <div class="mt-4">
                            <form action="{{ route('postPost') }}" method="post">
                                @csrf

                                <input type="hidden" name="topic_id" value="{{ $topic->id }}">

                                <label style="width: 100%">
                                    Žinutė

                                    <textarea
                                        class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}"
                                        rows="10"
                                        name="name"
                                        placeholder="Parašykite žinutę"
                                    ></textarea>

                                </label>

                                @if ($errors->has('name'))
                                    <div class="label label-danger my-3" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </div>
                                @endif

                                <div class="text-right">
                                    <button class="btn btn-success">Siųsti</button>
                                </div>

                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection