<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home2'); // Tiesiog /home rodo tą patį ką ir /

Route::post('/message/post', 'HomeController@postFormData')->name('postFormData');  // POST užklausos surinkimui


Route::post('/topic/post', 'HomeController@createTopic')->name('createTopic');

Route::post('/post/post', 'HomeController@postPost')->name('postPost');

Route::get('/topic/{id}', 'HomeController@topic')->name('topic');